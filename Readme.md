# API
## Todo

* [ ] error message for undefined routes
* [ ] log errors
* [x] error response with json
* [x] handle cors errors
* [x]  same error handling

## Example

```
{
    "purpose": "test",
    "value": "3",
    "date": "01.03.2001",
    "paid": false
}
```
