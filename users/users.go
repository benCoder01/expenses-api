package users

import (
	"net/http"
	"gopkg.in/mgo.v2"
	"encoding/json"
	"golang.org/x/crypto/bcrypt"

	"expenses-api/responses"
	"log"
	"gopkg.in/mgo.v2/bson"
	"github.com/dgrijalva/jwt-go"
	"os"
	"time"
)

type User struct {
	Username string `json:"username"`
	Password string `json:"password"`
}

type TokenResponse struct {
	Message string `json:"message"`
	Token   string `json:"token"`
}

func ensureIndex(c *mgo.Collection) {
	index := mgo.Index{
		Key:        []string{"username"},
		Unique:     true,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}

	err := c.EnsureIndex(index)

	if err != nil {
		panic(err)
	}
}

func SignIn(s *mgo.Session) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		session := s.Copy()
		defer session.Close()

		c := session.DB("expenses").C("users")
		ensureIndex(c)

		var request User

		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&request)

		if err != nil {
			responses.ErrorWithJSON(w, "Failed to parse JSON", http.StatusInternalServerError)
			return
		}

		var user User

		err = c.Find(bson.M{"username": request.Username}).One(&user)

		if err != nil {
			log.Print("Failed to find user: ", err)
			responses.ErrorWithJSON(w, "Failed to find user", http.StatusInternalServerError)
			return
		}

		if user.Username == "" {
			responses.ErrorWithJSON(w, "User not found", http.StatusNotFound)
			return
		}

		var tokenResponse TokenResponse
		tokenResponse.Message = "success"
		newToken, err := createJWTToken(user)

		if err != nil {
			responses.ErrorWithJSON(w, "Failed to create Token", http.StatusInternalServerError)
			return
		}

		tokenResponse.Token = newToken

		respBody, err := json.MarshalIndent(tokenResponse, "", "  ")

		if err != nil {
			responses.ErrorWithJSON(w, "Failed to parse JSON", http.StatusInternalServerError)
			return
		}

		responses.ResponseWithJSON(w, respBody, http.StatusOK)

	}
}

func createJWTToken(user User) (string, error) {
	/*
	jwt.StandardClaims{
			ExpiresAt: time.Now().Add(24 *time.Hour).Unix(),
		},
	 */

	rawToken := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"username": user.Username,
		"ExpiresAt": time.Now().Add(24 * time.Hour).Unix(),
	})

	token, err := rawToken.SignedString([]byte(os.Getenv("KEY")))

	if err != nil {
		return "", err
	}

	return token, nil
}

func SignUp(s *mgo.Session) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		session := s.Copy()
		defer session.Close()

		var user User

		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&user)

		if err != nil {
			responses.ErrorWithJSON(w, "Failed to parse JSON in request", http.StatusInternalServerError)
			return
		}

		hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), bcrypt.MinCost)

		if err != nil {
			responses.ErrorWithJSON(w, "Failed to hash password", http.StatusInternalServerError)
			return
		}

		user.Password = string(hash)

		c := session.DB("expenses").C("users")
		ensureIndex(c)

		err = c.Insert(user)

		if err != nil {

			if mgo.IsDup(err) {
				responses.ErrorWithJSON(w, "User exists", http.StatusBadRequest)
				return
			}

			responses.ErrorWithJSON(w, "Database error", http.StatusInternalServerError)
			return

		}

		// send response
		respBody, err := json.MarshalIndent(user, "", "  ")

		if err != nil {
			responses.ErrorWithJSON(w, "Failed to parse JSON in response", http.StatusInternalServerError)
			return
		}

		responses.ResponseWithJSON(w, respBody, http.StatusOK)

	}
}
