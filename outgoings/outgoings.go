package outgoings

import (
	"encoding/json"
	"net/http"
	"log"
	"gopkg.in/mgo.v2"
	"github.com/gorilla/mux"
	"gopkg.in/mgo.v2/bson"

	"expenses-api/responses"
)

// The Outgoing request without the authentication Token
type Outgoing struct {
	Id      bson.ObjectId `json:"id" bson:"_id,omitempty"`
	Purpose string        `json:"purpose"`
	Value   string        `json:"value"`
	Date    string        `json:"date"`
	Paid    bool          `json:"paid"`
}

// Set the key for the database
func ensureIndex(c *mgo.Collection) {
	index := mgo.Index{
		Key:        []string{"id"},
		Unique:     true,
		DropDups:   true,
		Background: true,
		Sparse:     true,
	}

	err := c.EnsureIndex(index)

	if err != nil {
		panic(err)
	}
}

// Returns all outgoings
func GetOutgoings(s *mgo.Session) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		session := s.Copy()
		defer session.Close()

		c := session.DB("expenses").C("outgoings")
		ensureIndex(c)

		var outgoings []Outgoing

		err := c.Find(bson.M{}).All(&outgoings)

		if err != nil {
			responses.ErrorWithJSON(w, "Failed get all outgoings ", http.StatusInternalServerError)
			log.Println("Failed get all outgoings: ", err)
			return
		}

		respBody, err := json.MarshalIndent(outgoings, "", "  ")

		if err != nil {
			responses.ErrorWithJSON(w, "Failed to parse JSON", http.StatusInternalServerError)
			return
		}

		responses.ResponseWithJSON(w, respBody, http.StatusOK)
	}
}

// Returns a specific outgoing. The Id is given in the params
func GetOutgoing(s *mgo.Session) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		session := s.Copy()
		defer session.Close()

		params := mux.Vars(r)

		c := session.DB("expenses").C("outgoings")
		ensureIndex(c)

		id := bson.ObjectIdHex(params["id"])

		// validate id
		if !id.Valid() {
			responses.ErrorWithJSON(w, "Not valid Id", http.StatusBadRequest)
			return
		}

		var outgoing Outgoing
		err := c.FindId(id).One(&outgoing)

		if err != nil {
			responses.ErrorWithJSON(w, "Failed to find outgoing", http.StatusInternalServerError)
			log.Println("Failed find Outgoing: ", err)
			return
		}

		if outgoing.Id.String() == "" {
			responses.ErrorWithJSON(w, "Outgoing not found", http.StatusNotFound)
			return
		}

		respBody, err := json.MarshalIndent(outgoing, "", "  ")

		if err != nil {
			responses.ErrorWithJSON(w, "Failed to parse JSON", http.StatusInternalServerError)
			return
		}

		responses.ResponseWithJSON(w, respBody, http.StatusOK)
	}
}

func AddOutgoing(s *mgo.Session) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		session := s.Copy()
		defer session.Close()

		var outgoing Outgoing
		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&outgoing)

		if err != nil {
			responses.ErrorWithJSON(w, "Failed to parse JSON", http.StatusInternalServerError)
			return
		}

		outgoing.Id = bson.NewObjectId()

		c := session.DB("expenses").C("outgoings")
		ensureIndex(c)

		err = c.Insert(outgoing)

		if err != nil {
			if mgo.IsDup(err) {
				responses.ErrorWithJSON(w, "Found duplicate", http.StatusBadRequest)
				return
			}

			responses.ErrorWithJSON(w, "Failed to insert outgoing", http.StatusInternalServerError)
			return

		}

		respBody, err := json.MarshalIndent(outgoing, "", " ")

		if err != nil {
			responses.ErrorWithJSON(w, "Failed to parse JSON", http.StatusInternalServerError)
			return
		}

		w.Header().Set("Location", r.URL.Path+"/"+outgoing.Id.String())
		responses.ResponseWithJSON(w, respBody, http.StatusCreated)
	}
}

func UpdateOutgoing(s *mgo.Session) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		session := s.Copy()
		defer session.Close()

		params := mux.Vars(r)

		c := session.DB("expenses").C("outgoings")
		ensureIndex(c)

		id := bson.ObjectIdHex(params["id"])

		var outgoing Outgoing

		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&outgoing)

		if err != nil {
			responses.ErrorWithJSON(w, "Failed to parse JSON", http.StatusBadRequest)
			return
		}

		err = c.UpdateId(id, &outgoing)
		if err != nil {
			switch err {
			default:
				responses.ErrorWithJSON(w, "Database error", http.StatusInternalServerError)
				log.Println("Failed update outgoing: ", err)
				return
			case mgo.ErrNotFound:
				responses.ErrorWithJSON(w, "Outgoing not found", http.StatusNotFound)
				return
			}
		}

		responses.ErrorWithJSON(w, "Id not found", http.StatusNoContent)
	}
}

func DeleteOutgoing(s *mgo.Session) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		session := s.Copy()
		defer session.Close()

		params := mux.Vars(r)

		c := session.DB("expenses").C("outgoings")
		ensureIndex(c)

		id := bson.ObjectIdHex(params["id"])

		err := c.RemoveId(id)

		if err != nil {
			switch err {
			default:
				responses.ErrorWithJSON(w, "Database error", http.StatusInternalServerError)
				return
			case mgo.ErrNotFound:
				responses.ErrorWithJSON(w, "Outgoing not found", http.StatusNotFound)
				return
			}
		}

		// TOOD Status Deleted

		responses.ErrorWithJSON(w, "Id not found", http.StatusNoContent)
	}
}
