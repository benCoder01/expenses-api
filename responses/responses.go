package responses

import (
	"net/http"
	"fmt"
	"encoding/json"
)

// Error response struct for valid JSON response
type ErrorResponse struct {
	Message string `json:"message"`
}

// Returns an error response as valid JSON with the given message and the status code
func ErrorWithJSON(w http.ResponseWriter, msg string, code int) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(code)

	var message ErrorResponse
	message.Message = msg

	respBody, err := json.MarshalIndent(message, "", "  ")

	if err != nil {
		w.Header().Set("Content-Type", "application/text; charset=utf-8")
		fmt.Fprintf(w, "{message: %q}", msg)
		return
	}

	w.Write(respBody)

}

// Returns a successful response to a request
func ResponseWithJSON(w http.ResponseWriter, json []byte, code int) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(code)
	w.Write(json)
}
