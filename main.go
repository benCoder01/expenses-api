package main

import (
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
	"github.com/gorilla/handlers"
	"gopkg.in/mgo.v2"

	"expenses-api/responses"
	"expenses-api/outgoings"
	"expenses-api/users"
	"github.com/dgrijalva/jwt-go"
	"github.com/subosito/gotenv"
	"fmt"
	"github.com/gorilla/context"
)

func ValidateMiddleware(next http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		authorizationHeader := r.Header.Get("authorization")
		if authorizationHeader != "" {
			token, err := jwt.Parse(authorizationHeader, func(token *jwt.Token) (interface{}, error) {

				if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
					return nil, fmt.Errorf("ERROR")
				}
				return []byte(os.Getenv("KEY")), nil

			})
			if err != nil {
				responses.ErrorWithJSON(w, "Internal error", http.StatusInternalServerError)
				return
			}
			if token.Valid {
				context.Set(r, "decoded", token.Claims)
				next(w, r)
			} else {
				responses.ErrorWithJSON(w, "Invalid authorization token", http.StatusBadRequest)
				return
			}
		} else {
			responses.ErrorWithJSON(w, "An authorization header is required", http.StatusBadRequest)
			return
		}
	})
}

func main() {
	session, err := mgo.Dial("mongodb://mongodb/exp")

	if err != nil {
		panic(err)
	}

	defer session.Close()

	session.SetMode(mgo.Monotonic, true)

	// load enviroment variables from .env
	gotenv.Load()

	// initialise Router
	r := mux.NewRouter()

	// Route handling

	// outgoings management
	go r.HandleFunc("/outgoings", ValidateMiddleware(outgoings.GetOutgoings(session))).Methods("GET")
	go r.HandleFunc("/outgoings/{id}", ValidateMiddleware(outgoings.GetOutgoing(session))).Methods("GET")
	go r.HandleFunc("/outgoings", ValidateMiddleware(outgoings.AddOutgoing(session))).Methods("POST")
	go r.HandleFunc("/outgoings/{id}", ValidateMiddleware(outgoings.UpdateOutgoing(session))).Methods("UPDATE")
	go r.HandleFunc("/outgoings/{id}", ValidateMiddleware(outgoings.DeleteOutgoing(session))).Methods("DELETE")

	// user management
	go r.HandleFunc("/users/sign-in", users.SignIn(session)).Methods("POST")
	go r.HandleFunc("/users/sign-up", users.SignUp(session)).Methods("POST")

	// apply logging middleware
	loggedRouter := handlers.LoggingHandler(os.Stdout, r)

	// set cors
	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With"})
	originsOk := handlers.AllowedOrigins([]string{os.Getenv("ORIGIN_ALLOWED")})
	methodsOk := handlers.AllowedMethods([]string{"GET", "PUT", "POST", "DELETE", "OPTIONS"})

	//start http server
	log.Fatal(http.ListenAndServe(":3000", handlers.CORS(originsOk, headersOk, methodsOk)(loggedRouter)))

}
