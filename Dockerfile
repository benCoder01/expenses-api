FROM golang:1.8

WORKDIR /go/src/expenses-api
COPY . .

RUN go install -v

CMD ["expenses-api"]